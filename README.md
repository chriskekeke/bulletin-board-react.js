# Bulletin board demo - React.js #

## About ##
This is a demo app that is built using components written with react.js

You can edit the notes and move them around! The notes are not stored so refreshing the browser will reset the app completely.

This demonstrates building simple components that interact with various libraries (jQuery and jQuery UI) as well as rendering data from an API (baconipsum).

The demo comes with packages to run a local server and to compile the jsx, should you need to.  Or you can view it on my site - [http://this-serious.com](http://this-serious.com/demos/bulletin-react/)

## Running the project locally ##
### Prerequisites ###
Install [Node.js](https://nodejs.org/) - I always do this using [Homebrew](http://brew.sh/)

```sh
$ ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
$ brew install node
```

Install jsx command line
```sh
$ npm install react-tools -g
```

### Install ###
```sh
$ npm install
$ jsx source/js js/app
```

### Running the app ###
```sh
$ npm start
```

Visit [http://localhost:3333/](http://localhost:3333/)